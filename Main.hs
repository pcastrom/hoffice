{-
 - Haskell Office Presentation
 - Author     : Paolo Castro
 - version    : 0.1_05-20-09
 - Description: Application to build html presentations. using xm
 -}
 
module Main where

import Presentation.Lexer
import Presentation.Parser
import System(getArgs)

main = do 
  args <- getArgs
  text <- readFile (head args)
  writeFile "presentation.html" (parse (alexScanTokens text))