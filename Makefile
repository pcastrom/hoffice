# HOffice Presentation MakeFile
ROOT_DIR=`pwd`
EXTRA_SRC_DIR=$(ROOT_DIR)/Presentation/ext
PRESENTATION_DIR=$(ROOT_DIR)/Presentation
LEXER_X=$(EXTRA_SRC_DIR)/Lexer.x
HAPPY_Y=$(EXTRA_SRC_DIR)/Parser.y
AG=$(EXTRA_SRC_DIR)/AttributeGrammar.ag
AG_HS=$(EXTRA_SRC_DIR)/AttributeGrammar.hs

# Actions

all:
	make build

alex:
	alex -o $(PRESENTATION_DIR)/Lexer.hs $(LEXER_X)

happy:
	happy -agc -o $(PRESENTATION_DIR)/Parser.hs $(HAPPY_Y)

ag:
	uuagc -a --module=Presentation.AttributeGrammar $(AG)
	mv $(AG_HS) $(PRESENTATION_DIR)/

delete-files:
	rm *.o *.hi
	rm $(PRESENTATION_DIR)/*.o $(PRESENTATION_DIR)/*.hi

clean:
	rm hop
	make delete-files

build:
	ghc -fglasgow-exts --make -o hop Main.hs
	make delete-files
