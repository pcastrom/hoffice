

-- UUAGC 0.9.39.1 (/Users/paolo/Development/haskell/HOffice-Presentati)
module Presentation.AttributeGrammar where


valid_effect ("Appear")    = "Appear"
valid_effect ("SlideDown") = "SlideDown"
valid_effect ("BlindDown") = "BlindDown"
valid_effect            _  = "Appear"

opposite ("Appear")    = "Fade"
opposite ("BlindDown") = "BlindUp"
opposite ("SlideDown") = "SlideUp"
opposite             _ = "Fade" 

link_to_start = "<div id=\"start\">\n"                                                 ++
                "<a href=\"javascript:void(0)\" "                                      ++
                "onclick=\"$('fade').appear({to:0.8});"                                ++
                "$('main').appear({queue: 'end'});\"> </a>\n</div>\n"

link_to_end id ef = "<div id=\"end\">\n"                                                        ++
                    "<a href=\"javascript:void(0)\" "                                           ++
                    "onclick=\"Effect." ++ (opposite ef) ++ "(\'"++ id ++"\');" ++
                    "Effect.Fade(\'fade\', {from : 0.8, queue: 'end'});\"> "              ++
                    "</a>\n</div>\n"

show_hide idS idH ef1 ef2 = "onclick=\"Effect." ++ ef1 ++ "(\'" ++ idS  ++"\',{queue: 'end'});" ++
                            "Effect." ++ (opposite ef2) ++ "(\'" ++ idH ++ "\',{queue:'front'});\""

link_next idN idH ef1 ef2 = "<div id=\"next\">\n"                                                           ++
                            "<a href=\"javascript:void(0)\" " ++ (show_hide idN idH ef1 ef2)  ++ ">"        ++
                            "</a>\n</div>\n"

link_prev idP idH ef1 ef2 = "<div id=\"prev\">\n"                                                           ++
                            "<a href=\"javascript:void(0)\" " ++ (show_hide idP idH ef1 ef2)  ++ ">"        ++
                            "</a>\n</div>\n"
                                        
create_index []     = ""
create_index (x:xs) = "<li>" ++ x ++ "</li>\n" ++ create_index xs

html_styles = "<link href=\"presentation-files/presentation.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />"

javascript  = "<script src=\"presentation-files/prototype.js\" type=\"text/javascript\"></script>"  ++
              "<script src=\"presentation-files/scriptaculous.js\" type=\"text/javascript\"></script>"
-- Animation ---------------------------------------------------
{-
   visit 0:
      synthesized attributes:
         anims                :  [String] 
         html                 :  String 
   alternatives:
      alternative Animation:
         child animation      : {String}
-}
data Animation  = Animation_Animation (String) 
                deriving ( Show)
-- cata
sem_Animation :: Animation  ->
                 T_Animation 
sem_Animation (Animation_Animation _animation )  =
    (sem_Animation_Animation _animation )
-- semantic domain
type T_Animation  = ( ( [String] ),( String ))
sem_Animation_Animation :: String ->
                           T_Animation 
sem_Animation_Animation animation_  =
    (let _lhsOanims :: ( [String] )
         _lhsOhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 65, column 15)
         _lhsOanims =
             let anim = valid_effect animation_
               in [anim]
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 110, column 15)
         _lhsOhtml =
             "animation here"
     in  ( _lhsOanims,_lhsOhtml))
-- Body --------------------------------------------------------
{-
   visit 0:
      synthesized attribute:
         html                 :  String 
   alternatives:
      alternative Body:
         child elements       : Elements 
-}
data Body  = Body_Body (Elements ) 
           deriving ( Show)
-- cata
sem_Body :: Body  ->
            T_Body 
sem_Body (Body_Body _elements )  =
    (sem_Body_Body (sem_Elements _elements ) )
-- semantic domain
type T_Body  = ( ( String ))
sem_Body_Body :: T_Elements  ->
                 T_Body 
sem_Body_Body elements_  =
    (let _lhsOhtml :: ( String )
         _elementsIhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 107, column 10)
         _lhsOhtml =
             "<div class=\"body\">\n" ++ _elementsIhtml ++ "</div>\n"
         ( _elementsIhtml) =
             elements_ 
     in  ( _lhsOhtml))
-- Element -----------------------------------------------------
{-
   visit 0:
      synthesized attribute:
         html                 :  String 
   alternatives:
      alternative Image:
         child image          : Image 
      alternative List:
         child list           : List 
      alternative Paragraph:
         child paragraph      : Paragraph 
-}
data Element  = Element_Image (Image ) 
              | Element_List (List ) 
              | Element_Paragraph (Paragraph ) 
              deriving ( Show)
-- cata
sem_Element :: Element  ->
               T_Element 
sem_Element (Element_Image _image )  =
    (sem_Element_Image (sem_Image _image ) )
sem_Element (Element_List _list )  =
    (sem_Element_List (sem_List _list ) )
sem_Element (Element_Paragraph _paragraph )  =
    (sem_Element_Paragraph (sem_Paragraph _paragraph ) )
-- semantic domain
type T_Element  = ( ( String ))
sem_Element_Image :: T_Image  ->
                     T_Element 
sem_Element_Image image_  =
    (let _lhsOhtml :: ( String )
         _imageIhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 100, column 15)
         _lhsOhtml =
             _imageIhtml
         ( _imageIhtml) =
             image_ 
     in  ( _lhsOhtml))
sem_Element_List :: T_List  ->
                    T_Element 
sem_Element_List list_  =
    (let _lhsOhtml :: ( String )
         _listIhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 99, column 15)
         _lhsOhtml =
             _listIhtml
         ( _listIhtml) =
             list_ 
     in  ( _lhsOhtml))
sem_Element_Paragraph :: T_Paragraph  ->
                         T_Element 
sem_Element_Paragraph paragraph_  =
    (let _lhsOhtml :: ( String )
         _paragraphIhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 98, column 15)
         _lhsOhtml =
             _paragraphIhtml
         ( _paragraphIhtml) =
             paragraph_ 
     in  ( _lhsOhtml))
-- Elements ----------------------------------------------------
{-
   visit 0:
      synthesized attribute:
         html                 :  String 
   alternatives:
      alternative Cons:
         child hd             : Element 
         child tl             : Elements 
      alternative Nil:
-}
type Elements  = [Element ]
-- cata
sem_Elements :: Elements  ->
                T_Elements 
sem_Elements list  =
    (Prelude.foldr sem_Elements_Cons sem_Elements_Nil (Prelude.map sem_Element list) )
-- semantic domain
type T_Elements  = ( ( String ))
sem_Elements_Cons :: T_Element  ->
                     T_Elements  ->
                     T_Elements 
sem_Elements_Cons hd_ tl_  =
    (let _lhsOhtml :: ( String )
         _hdIhtml :: ( String )
         _tlIhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 103, column 10)
         _lhsOhtml =
             _hdIhtml ++ _tlIhtml
         ( _hdIhtml) =
             hd_ 
         ( _tlIhtml) =
             tl_ 
     in  ( _lhsOhtml))
sem_Elements_Nil :: T_Elements 
sem_Elements_Nil  =
    (let _lhsOhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 104, column 10)
         _lhsOhtml =
             ""
     in  ( _lhsOhtml))
-- Image -------------------------------------------------------
{-
   visit 0:
      synthesized attribute:
         html                 :  String 
   alternatives:
      alternative Image:
         child image          : {String}
-}
data Image  = Image_Image (String) 
            deriving ( Show)
-- cata
sem_Image :: Image  ->
             T_Image 
sem_Image (Image_Image _image )  =
    (sem_Image_Image _image )
-- semantic domain
type T_Image  = ( ( String ))
sem_Image_Image :: String ->
                   T_Image 
sem_Image_Image image_  =
    (let _lhsOhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 82, column 11)
         _lhsOhtml =
             "<div id=\"image\">\n<img src=\"presentation-files/" ++ image_ ++ "\">\n</div>\n"
     in  ( _lhsOhtml))
-- Item --------------------------------------------------------
{-
   visit 0:
      synthesized attribute:
         html                 :  String 
   alternatives:
      alternative Item:
         child item           : {String}
-}
data Item  = Item_Item (String) 
           deriving ( Show)
-- cata
sem_Item :: Item  ->
            T_Item 
sem_Item (Item_Item _item )  =
    (sem_Item_Item _item )
-- semantic domain
type T_Item  = ( ( String ))
sem_Item_Item :: String ->
                 T_Item 
sem_Item_Item item_  =
    (let _lhsOhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 85, column 11)
         _lhsOhtml =
             "<li>" ++ item_ ++ "</li>\n"
     in  ( _lhsOhtml))
-- Items -------------------------------------------------------
{-
   visit 0:
      synthesized attribute:
         html                 :  String 
   alternatives:
      alternative Cons:
         child hd             : Item 
         child tl             : Items 
      alternative Nil:
-}
type Items  = [Item ]
-- cata
sem_Items :: Items  ->
             T_Items 
sem_Items list  =
    (Prelude.foldr sem_Items_Cons sem_Items_Nil (Prelude.map sem_Item list) )
-- semantic domain
type T_Items  = ( ( String ))
sem_Items_Cons :: T_Item  ->
                  T_Items  ->
                  T_Items 
sem_Items_Cons hd_ tl_  =
    (let _lhsOhtml :: ( String )
         _hdIhtml :: ( String )
         _tlIhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 88, column 10)
         _lhsOhtml =
             _hdIhtml ++ _tlIhtml
         ( _hdIhtml) =
             hd_ 
         ( _tlIhtml) =
             tl_ 
     in  ( _lhsOhtml))
sem_Items_Nil :: T_Items 
sem_Items_Nil  =
    (let _lhsOhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 89, column 10)
         _lhsOhtml =
             ""
     in  ( _lhsOhtml))
-- List --------------------------------------------------------
{-
   visit 0:
      synthesized attribute:
         html                 :  String 
   alternatives:
      alternative List:
         child items          : Items 
-}
data List  = List_List (Items ) 
           deriving ( Show)
-- cata
sem_List :: List  ->
            T_List 
sem_List (List_List _items )  =
    (sem_List_List (sem_Items _items ) )
-- semantic domain
type T_List  = ( ( String ))
sem_List_List :: T_Items  ->
                 T_List 
sem_List_List items_  =
    (let _lhsOhtml :: ( String )
         _itemsIhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 92, column 10)
         _lhsOhtml =
             "<ul>\n" ++ _itemsIhtml ++ "</ul>\n"
         ( _itemsIhtml) =
             items_ 
     in  ( _lhsOhtml))
-- Paragraph ---------------------------------------------------
{-
   visit 0:
      synthesized attribute:
         html                 :  String 
   alternatives:
      alternative Paragraph:
         child paragraph      : {String}
-}
data Paragraph  = Paragraph_Paragraph (String) 
                deriving ( Show)
-- cata
sem_Paragraph :: Paragraph  ->
                 T_Paragraph 
sem_Paragraph (Paragraph_Paragraph _paragraph )  =
    (sem_Paragraph_Paragraph _paragraph )
-- semantic domain
type T_Paragraph  = ( ( String ))
sem_Paragraph_Paragraph :: String ->
                           T_Paragraph 
sem_Paragraph_Paragraph paragraph_  =
    (let _lhsOhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 95, column 15)
         _lhsOhtml =
             "<p>\n" ++ paragraph_ ++ "</p>\n"
     in  ( _lhsOhtml))
-- Presentation ------------------------------------------------
{-
   visit 0:
      synthesized attribute:
         html                 :  String 
   alternatives:
      alternative Presentation:
         child title          : Title 
         child slides         : Slides 
-}
data Presentation  = Presentation_Presentation (Title ) (Slides ) 
                   deriving ( Show)
-- cata
sem_Presentation :: Presentation  ->
                    T_Presentation 
sem_Presentation (Presentation_Presentation _title _slides )  =
    (sem_Presentation_Presentation (sem_Title _title ) (sem_Slides _slides ) )
-- semantic domain
type T_Presentation  = ( ( String ))
sem_Presentation_Presentation :: T_Title  ->
                                 T_Slides  ->
                                 T_Presentation 
sem_Presentation_Presentation title_ slides_  =
    (let _lhsOhtml :: ( String )
         _titleIhtml :: ( String )
         _titleItitles :: ( [String] )
         _slidesIanims :: ( [String] )
         _slidesIhtml :: ( String )
         _slidesItitles :: ( [String] )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 135, column 18)
         _lhsOhtml =
             "<div id=\"main\" class=\"white\" "                         ++
             "style=\"display:none;\">\n<div class=\"content\">\n"       ++
             _titleIhtml ++ (link_next "index" "main" "Appear" "Appear") ++
             "</div></div>\n"                                            ++
             "<div id=\"index\" class=\"white\" "                        ++
             "style=\"display:none;\">\n<div class=\"content\">\n"       ++
             "<h1> Index </h1>\n"                                        ++
             "<ul>\n" ++ (create_index _slidesItitles) ++ "</ul>\n"      ++
             (link_prev "main" "index" "Appear" "Appear")                ++
             (link_next (head _slidesItitles) "index"
               (head _slidesIanims) "Appear")                            ++
             "</div></div>\n"                                            ++
             "<div id=\""++ (head _slidesItitles)                        ++
             "\" class=\"white\" style=\"display:none;\">\n"             ++
             "<div class=\"content\">\n"                                 ++
             (link_prev "index" (head _slidesItitles) "Appear"
               (head _slidesIanims))                                     ++
             _slidesIhtml                                                ++
             "<div id=\"fade\" class=\"black\" "                         ++
             "style=\"display:none;\"></div>\n"
         ( _titleIhtml,_titleItitles) =
             title_ 
         ( _slidesIanims,_slidesIhtml,_slidesItitles) =
             slides_ 
     in  ( _lhsOhtml))
-- Root --------------------------------------------------------
{-
   visit 0:
      synthesized attribute:
         html                 :  String 
   alternatives:
      alternative Root:
         child presentation   : Presentation 
-}
data Root  = Root_Root (Presentation ) 
           deriving ( Show)
-- cata
sem_Root :: Root  ->
            T_Root 
sem_Root (Root_Root _presentation )  =
    (sem_Root_Root (sem_Presentation _presentation ) )
-- semantic domain
type T_Root  = ( ( String ))
sem_Root_Root :: T_Presentation  ->
                 T_Root 
sem_Root_Root presentation_  =
    (let _lhsOhtml :: ( String )
         _presentationIhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 158, column 10)
         _lhsOhtml =
             "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"         ++
             "<html>\n<head>\n<title>Presentation</title>\n"                               ++
             html_styles ++ javascript ++ "</head>\n" ++
             "<body>\n" ++ link_to_start ++
             _presentationIhtml ++ "</body>\n</html>\n"
         ( _presentationIhtml) =
             presentation_ 
     in  ( _lhsOhtml))
-- Slide -------------------------------------------------------
{-
   visit 0:
      synthesized attributes:
         anims                :  [String] 
         html                 :  String 
         titles               :  [String] 
   alternatives:
      alternative Slide:
         child animation      : Animation 
         child title          : Title 
         child body           : Body 
-}
data Slide  = Slide_Slide (Animation ) (Title ) (Body ) 
            deriving ( Show)
-- cata
sem_Slide :: Slide  ->
             T_Slide 
sem_Slide (Slide_Slide _animation _title _body )  =
    (sem_Slide_Slide (sem_Animation _animation ) (sem_Title _title ) (sem_Body _body ) )
-- semantic domain
type T_Slide  = ( ( [String] ),( String ),( [String] ))
sem_Slide_Slide :: T_Animation  ->
                   T_Title  ->
                   T_Body  ->
                   T_Slide 
sem_Slide_Slide animation_ title_ body_  =
    (let _lhsOtitles :: ( [String] )
         _lhsOanims :: ( [String] )
         _lhsOhtml :: ( String )
         _animationIanims :: ( [String] )
         _animationIhtml :: ( String )
         _titleIhtml :: ( String )
         _titleItitles :: ( [String] )
         _bodyIhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 53, column 11)
         _lhsOtitles =
             _titleItitles
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 70, column 11)
         _lhsOanims =
             _animationIanims
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 113, column 11)
         _lhsOhtml =
             _titleIhtml ++ _bodyIhtml
         ( _animationIanims,_animationIhtml) =
             animation_ 
         ( _titleIhtml,_titleItitles) =
             title_ 
         ( _bodyIhtml) =
             body_ 
     in  ( _lhsOanims,_lhsOhtml,_lhsOtitles))
-- Slides ------------------------------------------------------
{-
   visit 0:
      synthesized attributes:
         anims                :  [String] 
         html                 :  String 
         titles               :  [String] 
   alternatives:
      alternative Cons:
         child hd             : Slide 
         child tl             : Slides 
      alternative Nil:
-}
type Slides  = [Slide ]
-- cata
sem_Slides :: Slides  ->
              T_Slides 
sem_Slides list  =
    (Prelude.foldr sem_Slides_Cons sem_Slides_Nil (Prelude.map sem_Slide list) )
-- semantic domain
type T_Slides  = ( ( [String] ),( String ),( [String] ))
sem_Slides_Cons :: T_Slide  ->
                   T_Slides  ->
                   T_Slides 
sem_Slides_Cons hd_ tl_  =
    (let _lhsOtitles :: ( [String] )
         _lhsOanims :: ( [String] )
         _lhsOhtml :: ( String )
         _hdIanims :: ( [String] )
         _hdIhtml :: ( String )
         _hdItitles :: ( [String] )
         _tlIanims :: ( [String] )
         _tlIhtml :: ( String )
         _tlItitles :: ( [String] )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 56, column 10)
         _lhsOtitles =
             _hdItitles ++ _tlItitles
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 73, column 10)
         _lhsOanims =
             _hdIanims ++ _tlIanims
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 116, column 11)
         _lhsOhtml =
             _hdIhtml ++
             if _tlItitles == [] then
               (link_to_end (head _hdItitles) (head _hdIanims)) ++ "</div>\n</div>\n"
             else
               (link_next (head _tlItitles) (head _hdItitles)
                  (head _tlIanims) (head _hdIanims))                         ++
               "</div>\n</div>\n"                                            ++
               "<div id=\"" ++ (head _tlItitles)  ++ "\" class=\"white\" "   ++
               "style=\"display:none;\">\n<div class=\"content\">"           ++
               (link_prev (head _hdItitles) (head _tlItitles)
                  (head _hdIanims) (head _tlIanims))                         ++
               _tlIhtml
         ( _hdIanims,_hdIhtml,_hdItitles) =
             hd_ 
         ( _tlIanims,_tlIhtml,_tlItitles) =
             tl_ 
     in  ( _lhsOanims,_lhsOhtml,_lhsOtitles))
sem_Slides_Nil :: T_Slides 
sem_Slides_Nil  =
    (let _lhsOtitles :: ( [String] )
         _lhsOanims :: ( [String] )
         _lhsOhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 57, column 10)
         _lhsOtitles =
             []
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 74, column 10)
         _lhsOanims =
             []
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 129, column 11)
         _lhsOhtml =
             ""
     in  ( _lhsOanims,_lhsOhtml,_lhsOtitles))
-- Title -------------------------------------------------------
{-
   visit 0:
      synthesized attributes:
         html                 :  String 
         titles               :  [String] 
   alternatives:
      alternative Title:
         child title          : {String}
-}
data Title  = Title_Title (String) 
            deriving ( Show)
-- cata
sem_Title :: Title  ->
             T_Title 
sem_Title (Title_Title _title )  =
    (sem_Title_Title _title )
-- semantic domain
type T_Title  = ( ( String ),( [String] ))
sem_Title_Title :: String ->
                   T_Title 
sem_Title_Title title_  =
    (let _lhsOtitles :: ( [String] )
         _lhsOhtml :: ( String )
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 50, column 11)
         _lhsOtitles =
             [title_]
         -- "/Users/paolo/Development/haskell/HOffice-Presentation/Presentation/ext/AttributeGrammar.ag"(line 132, column 11)
         _lhsOhtml =
             "<h1>" ++ title_ ++ "</h1>\n"
     in  ( _lhsOhtml,_lhsOtitles))