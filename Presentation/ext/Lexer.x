{
module Presentation.Lexer where
}

%wrapper "basic"

$endl     = [\n]
$white    = [\ \t]
$alphanum = [a-zA-Z0-9]
$special  = [\'\_\-\@\#\:\(\)\[\]\.\,\;\$\%\&\?\!]

@keyword  = presentation | slide | title | body | paragraph | list | image | animation
@text     = ($alphanum | $white | $special)+

lexer :-

  $endl+                 ;
  $white+                ;
  @keyword               { \s -> Keyword s              }
  \"@text\"              { \s -> Text ((tail . init) s) }
  \{                     { \s -> Open                   }
  \}                     { \s -> Close                  }
  \=                     { \s -> Assign                 }

{
data Token =
  Keyword   String |
  Text      String |
  Open             |
  Close            |
  Assign
  deriving(Eq,Show)

scan = alexScanTokens
}