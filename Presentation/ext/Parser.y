{
module Presentation.Parser where

import Presentation.Lexer
import Presentation.AttributeGrammar
}

%name parse Root
%tokentype { Token }
%error     { pError }

%token
  prese { Keyword "presentation" }
  slide { Keyword "slide"        }
  title { Keyword "title"        }
  body  { Keyword "body"         }
  list  { Keyword "list"         }
  para  { Keyword "paragraph"    }
  image { Keyword "image"        }
  anim  { Keyword "animation"    }
  text  { Text      $$           }
  '{'   { Open                   }
  '}'   { Close                  }
  '='   { Assign                 }

%%

Root         : Presentation                         { sem_Root_Root $1                            }

Presentation : prese '{' Title Slides '}'           { sem_Presentation_Presentation $3 $4         }

Title        : title '=' text                       { sem_Title_Title $3                          } 

Slides       : Slide Slides                         { sem_Slides_Cons $1 $2                       }
             | {- empty -}                          { sem_Slides_Nil                              }

Slide        : slide '{' Animation Title Body '}'   { sem_Slide_Slide $3 $4 $5                    }

Animation    : anim '=' text                        { sem_Animation_Animation $3                  }

Body         : body '{' Elements '}'                { sem_Body_Body $3                            }

Elements     : Element Elements                     { sem_Elements_Cons $1 $2                     }
             | {- empty -}                          { sem_Elements_Nil                            }

Element      : Paragraph                            { sem_Element_Paragraph $1                    }
             | List                                 { sem_Element_List      $1                    }
             | Image                                { sem_Element_Image     $1                    }

Paragraph    : para '=' text                        { sem_Paragraph_Paragraph $3                  }

List         : list '{' Items '}'                   { sem_List_List $3                            }

Items        : Item Items                           { sem_Items_Cons $1 $2                        }
             | {- empty -}                          { sem_Items_Nil                               }

Item         : text                                 { sem_Item_Item $1                            }

Image        : image '=' text                       { sem_Image_Image $3                          }

{
pError ts = error (show ts ++ "Parse Errors")
}